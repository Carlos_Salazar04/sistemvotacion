package Entidad;

import java.util.Random;
import ufps.util.colecciones_seed.Pila;

public class CentroVotacion {

    private int id_centro;

    private String nombreCentro;

    private String direccion;
    
    private Pila<Mesa> mesas=new Pila();
    
    private int cantidadSufragantes;

    public CentroVotacion() {
    
    }

    public CentroVotacion(int id_centro, String nombreCentro, String direccion, int cantidadSufragantes) {
        this.id_centro = id_centro;
        this.nombreCentro = nombreCentro;
        this.direccion = direccion;
        this.cantidadSufragantes = cantidadSufragantes;
    }
    
    public int getId_centro() {
        return id_centro;
    }

    public void setId_centro(int id_centro) {
        this.id_centro = id_centro;
    }

    public String getNombreCentro() {
        return nombreCentro;
    }

    public void setNombreCentro(String nombreCentro) {
        this.nombreCentro = nombreCentro;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Pila<Mesa> getMesas() {
        return mesas;
    }

    public void setMesas(Pila<Mesa> mesas) {
        this.mesas = mesas;
    }

    public int getCantidadSufragantes() {
        return cantidadSufragantes;
    }

    public void setCantidadSufragantes(int cantidadSufragantes) {
        this.cantidadSufragantes = cantidadSufragantes;
    }
        
    public void crearMesas(int cantidadMesas,int idInicial) {
        for(int i = 1; i < cantidadMesas; i++) {
            this.mesas.apilar(new Mesa(i + idInicial));
        }
    }
    
    public void agregarPersona(Persona persona) {
        if (this.mesas.esVacia() || this.cantidadSufragantes == 0) {
            throw new RuntimeException("No se pueden agregar más personas a este centro de votación");
        }
        Random rnd = new Random();
        Pila<Mesa> pilaAuxiliar = new Pila();
        int valorEntero = (int) (rnd.nextDouble() * this.mesas.getTamanio());
        while(valorEntero-- > 0) {
            pilaAuxiliar.apilar(this.mesas.desapilar());
        }
        
        Mesa myMesa = this.mesas.desapilar();
        if (persona.getEdad() < 60 && myMesa.faltanJurados()) {
            myMesa.agregarJurado(persona);
            persona.setEsJurado(true);
        }
        else {
            myMesa.getSufragantes().enColar(persona);
            persona.setEsSufragante(true);
        }
        
        this.mesas.apilar(myMesa);
        while(!pilaAuxiliar.esVacia()) {
            this.mesas.apilar(pilaAuxiliar.desapilar());
        }
    }

    @Override
    public String toString() {
        return this.id_centro + " " + this.nombreCentro;
    }
    
}
