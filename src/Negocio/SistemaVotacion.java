package Negocio;
import Entidad.*;
import java.util.Random;
import ufps.util.colecciones_seed.*;
import ufps.util.varios.ArchivoLeerURL;
public class SistemaVotacion {
    
    private ListaCD<Departamento> dptos=new ListaCD();
    private ListaCD<Persona> personas=new ListaCD();
    private Cola<Notificacion> notificaciones=new Cola();

    public SistemaVotacion() {
    }

    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public void setDptos(ListaCD<Departamento> dptos) {
        this.dptos = dptos;
    }

    public ListaCD<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ListaCD<Persona> personas) {
        this.personas = personas;
    }

    public Cola<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(Cola<Notificacion> notificaciones) {
        this.notificaciones = notificaciones;
    }
    
    /*
    * Punto 1
    */
    public void cargarPersonas(String url) {
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object obj[] = file.leerArchivo();
        
        for(int i = 1; i < obj.length; i++) {
            //cedula;nombre;fechanacimiento;id_municipio_inscripcion;email
            String linea = obj[i].toString();
            //100200;nombre100200;1958-12-3;5;nombre100200@sindir.co
            String datos[] = linea.split(";");
            // datos[0] = 100200, datos[1] = nombre100200, datos[2] = 1958-12-3, datos[3] = 5, datos[4] = nombre100200@sindir.co 
            Persona nuevo = new Persona(Long.parseLong(datos[0]), datos[1], datos[2], Short.parseShort(datos[3]), datos[4]);
            this.personas.insertarAlFinal(nuevo);
        }
    }
    
    /*
    * Punto 2
    */
    public void cargarDepartamentos(String url) {
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object obj[] = file.leerArchivo();
        
        for(int i = 1; i < obj.length; i++) {
            //id_dpto;Nombre Departamento
            String linea = obj[i].toString();
            //1;Antioquia
            String datos[] = linea.split(";");
            // datos[0] = 1, datos[1] = Antioquia
            Departamento nuevo = new Departamento(Integer.parseInt(datos[0]), datos[1]);
            this.dptos.insertarAlFinal(nuevo);
        }
    }
    
    /*
    * Punto 3
    */
    public void cargarMunicipios(String url) {
        if (this.dptos.esVacia()) {
            throw new RuntimeException("No se pueden cargar municipios si no existen departamentos");
        }
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object obj[] = file.leerArchivo();
        
        for(int i = 1; i < obj.length; i++) {
            //id_dpto;id_municipio;nombreMunicipio
            String linea = obj[i].toString();
            //1;3;Abejorral
            String datos[] = linea.split(";");
            // datos[0] = 1, datos[1] = 3, datos[2] = Abejorral
            Municipio nuevo = new Municipio(Integer.parseInt(datos[1]), datos[2]);
            Departamento dpto = this.getDepartamento(Integer.parseInt(datos[0]));
            if (dpto == null) {
                throw new RuntimeException("No se pueden cargar municipios, no existe el departamento asosiado");
            }
            dpto.getMunicipios().insertarAlFinal(nuevo);
        }
    }
    
    private Departamento getDepartamento(int id) {
        for (Departamento dpto : this.dptos) {
            if (dpto.getId_dpto() == id) {
                return dpto;
            }
        }
        return null;
    }
    
    /*
    * Punto 4
    */
    public void cargarCentrosYMesasDeVotacion(String url) {
        if (this.dptos.esVacia()) {
            throw new RuntimeException("No se pueden cargar personas si no existen departamentos");
        }
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object obj[] = file.leerArchivo();
        int cantidadMesasRegistradas = 0;
        
        for(int i = 1; i < obj.length; i++) {
            //id_centro;nombreCentro;direccion;id_municipio;cantMesas;maximoSufragantes
            String linea = obj[i].toString();
            //1;centro1;direccion_centro1;3;2;6
            String datos[] = linea.split(";");
            // datos[0] = 1, datos[1] = centro1, datos[2] = direccion_centro1, datos[2] = 3, datos[3] = 2, datos[4] = 6
            Municipio m = this.getMunicipio(Integer.parseInt(datos[2]));
            if (m == null) {
                throw new RuntimeException("No se puede cargar un centro de votación en un municipio inexistente");
            }
            CentroVotacion nuevo = new CentroVotacion(Integer.parseInt(datos[0]), datos[1], datos[2], Integer.parseInt(datos[4]));
            nuevo.crearMesas(Integer.parseInt(datos[3]), cantidadMesasRegistradas);
            cantidadMesasRegistradas += Integer.parseInt(datos[3]);
            m.getCentros().insertarAlFinal(nuevo);
        }
    }
    
    private Municipio getMunicipio(int id) {
        for (Departamento dpto : this.dptos) {
            Municipio m = dpto.getMunicipioId(id);
            if (m != null) return m;
        }
        return null;
    } 
    
    /*
    * Punto 5
    */
    public void crearNotificaciones() {
        Random rnd = new Random();
        for (Persona persona : this.personas) {
            Municipio myMunicipio = this.getMunicipio(persona.getId_Municipio_inscripcion());
            if (!myMunicipio.getCentros().esVacia()) {
                int indice = (int) (rnd.nextDouble() * myMunicipio.getCentros().getTamanio());
                CentroVotacion myCentroVotacion = myMunicipio.getCentros().get(indice);
                try {
                    myCentroVotacion.agregarPersona(persona);
                }
                catch(RuntimeException e) {
                    System.err.println("No se pudo agregar una persona en el centro de votación" + myCentroVotacion);
                }
            }
        }
    }
    
    /*
    * Punto 6
    */
    public String getMesasYPuestosAnulados() {
        return null;
    }
    
    
    /*
    * Punto 7
    */
    public String getPersonasSinCentroDeVotacion() {
        String msg = "";
        for (Persona persona : this.personas) {
            if (!persona.isEsJurado() || !persona.isEsSufragante()) {
                msg += persona.toString() + " ";
            }
        }
        return msg;
    }
}
